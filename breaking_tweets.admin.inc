<?php

/**
 * Admin page for Breaking Tweets
**/

/** 
 * Implements hook_form()
**/

function breaking_tweets_config($form, &$form_state) {
  global $conf;

  $site_name = $conf['site_name'];
  
  drupal_add_css(drupal_get_path('module', 'breaking-tweets').'/css/breaking-admin.css');
  
  $form['query'] = array(
    '#type' => 'textfield',
    '#title' => t('Search Query'),
    '#description' => t('The query you want to search on. Can be any valid search query for Twitter.com'),
    '#default_value' => variable_get('breaking_tweets_query', $site_name),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['search-help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Defining your Search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['items'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Items'),
    '#description' => t('Number of items you want to pull from Twitter at a time. Minimum 1'),
    '#default_value' => variable_get('breaking_tweets_items', 5),
    '#size' => 5,
    '#maxlength' => 3,
    '#required' => TRUE,
  );
  $form['sbmit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  $form['search-help']['general'] = array(
    '#type' => 'textfield',
    '#title' => t('General Search'),
    '#description' => t('A search that must contain all phrases, but not necessarily in the order they are given; simply type something in! For instance: Hello World'),
    '#disabled' => TRUE,
  );
  $form['search-help']['exact'] = array(
    '#type' => 'textfield',
    '#title' => t('Specific Search'),
    '#description' => t('An search for an exact phrase; enclose what you are searching for in quotes. For instance: "Hello World"'),
    '#disabled' => TRUE,
  );
  $form['search-help']['multiple'] = array(
    '#type' => 'textfield',
    '#title' => t('Multiple Searches'),
    '#description' => t('A search for multiple items; separate each search term with OR. For instance: DrupalCon OR Hello World'),
    '#disabled' => TRUE,
  );
  $form['search-help']['multiple'] = array(
    '#type' => 'textfield',
    '#title' => t('Excluding Keywords'),
    '#description' => t('Exclude tweets with a given keyword: ad a - before the keyword. For instance: -NerdFun'),
    '#disabled' => TRUE,
  );
  $form['search-help']['hashtag'] = array(
    '#type' => 'textfield',
    '#title' => t('Hashtag Search'),
    '#description' => t('A search for a hashtag; add a # in front of your search. For instance: #DrupalCon'),
    '#disabled' => TRUE,
  );
  $form['search-help']['from-users'] = array(
    '#type' => 'textfield',
    '#title' => t('From User'),
    '#description' => t('Restrict search to those sent by a given user; add from: before a username. For instance: from:DrupalCon'),
    '#disabled' => TRUE,
  );
  $form['search-help']['to-user'] = array(
    '#type' => 'textfield',
    '#title' => t('To User'),
    '#description' => t('Restrict search to those sent to a given user; add to: before a username. For instance: to:DrupalCon'),
    '#disabled' => TRUE,
  );
  $form['search-help']['mentions'] = array(
    '#type' => 'textfield',
    '#title' => t('Mentioning User'),
    '#description' => t('Restrict search to those mentioning a given user; add @ before a username. For instance: @DrupalCon'),
    '#disabled' => TRUE,
  );
  $form['search-help']['mentions'] = array(
    '#type' => 'textfield',
    '#title' => t('Mentioning User'),
    '#description' => t('Restrict search to those mentioning a given user; add @ before a username. For instance: @DrupalCon'),
    '#disabled' => TRUE,
  );
  $form['search-help']['all-tegether'] = array(
    '#type' => 'textfield',
    '#title' => t('Multifaceted Searches'),
    '#description' => t('To combine multiple searches, use OR to combine searches that do not have to be part of each result and spaces for searches that need to be. For instance, for a list of tweets that contain either the exact phrase "Hello World" or the hashtag #DrupalCon, mention @Drupal, and are from either @DrupalCon or Dries, we can construct that search as follows: "Hello World" OR #DrupalCon @Drupal from:DrupalCon OR from:Dries.'),
    '#disabled' => TRUE,
  );
  
  return $form;
}

/**
 * Implements _form_validate()
**/

function  breaking_tweets_config_validate(&$elements, &$form_state) {
  $items = $elements['items']['#value'];
  if (is_nan($items)) {
    form_set_error($elements['items'], 'The number of items must be a number');
  } else {
    if ($items < 1) {
      form_set_error($elements['items'], 'The number of items must be greater than or equal to 1');
    }
  }
}

/**
 * Implements _form_submit()
**/

function  breaking_tweets_config_submit($form, &$form_state) {
  $query = filter_xss($form_state['values']['query']);
  $items = $form_state['values']['items'];
  
  variable_set('breaking_tweets_query', $query);
  variable_set('breaking_tweets_items', $items);
  
  drupal_set_message('Your settings have been saved');
}