<?php
// $Id: twitter-pull-listing.tpl.php,v 1.2.2.3 2011/01/11 02:49:27 inadarei Exp $

/**
 * @file
 * Theme template for a list of tweets.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $tweets, where each tweet object has:
 *   $tweet->id
 *   $tweet->username
 *   $tweet->userphoto
 *   $tweet->text
 *   $tweet->timestamp
 *
 * 2) $twitkey string containing initial keyword.
 *
 * 3) $title
 *
 */
?>
<?php 
  $mpath = drupal_get_path('module', 'breaking_tweets').'/';
  drupal_add_css($mpath.'css/breaking-tweets.css');  
  drupal_add_js($mpath.'js/breaking-tweets.js');
  $i = 1;
?>

<div class="tweets-pulled-wrapper">

  <?php if (is_array($tweets)): ?>
    <?php $tweet_count = count($tweets); ?>
    
    <!-- <ul class="tweets-pulled-listing tweet-count-<?php print $tweet_count; ?>"> -->
    <ul class="tweets-pulled-listing">
    <?php foreach ($tweets as $tweet_key => $tweet): ?>
      <?php if($i == 1) { ?>
        <li class="tweet tweet-<?php print $i; ?> first">
      <?php $i++; } else if($i == $tweet_count) { ?>
        <li class="tweet tweet-<?php print $i; ?> last">
      <?php $i++; } else { ?>
        <li class="tweet tweet-<?php print $i; ?>">
      <?php $i++; } ?>
      
        <span class="tweet-author"><?php print l($tweet->username, 'http://twitter.com/' . $tweet->username); ?></span>
        <span class="tweet-text"><?php print twitter_pull_add_links($tweet->text); ?></span>
        <span class="tweet-time"><?php print ' | '.l($tweet->time_ago, 'http://twitter.com/' . $tweet->username . '/status/' . $tweet->id);?></span>

        <?php if ($tweet_key < $tweet_count - 1): ?>
          <div class="tweet-divider"></div>
        <?php endif; ?>
        
      </li>
    <?php endforeach; ?>
    </ul>
  <?php endif; ?>
</div>
