jQuery(function() {
  $ = jQuery;
  
  var tweet_count = $('.tweets-pulled-listing .tweet').length;
  
  // Initialize the hiding of the tweets
  $('.tweet').each(function() {
    $(this).css('display', 'none');
    if ($(this).hasClass('first')) {
      $(this).css('display', 'block');
      $(this).addClass('active');
    };
  });
  
  // Start the tweet cycle
  breaking_tweets_cycle($('.tweet'));
  
  
  
});

function breaking_tweets_cycle(obj) {
  var obj = jQuery(obj);
  var i = 2;
  
  setInterval(function() {
    obj.each(function(index) {
      // Remove the Active class and hide      
      if ($(this).hasClass('active')) {
        if (!$(this).hasClass('tweet-'+i)) {
          $(this).removeClass('active');
          $(this).css('display', 'none');
        }
      }
      
      // Add Active class to active tweet
      if ($(this).hasClass('tweet-'+i)) {
        $(this).css('display', 'block');
        $(this).addClass('active');
      }
      
    });
    
    // Increment counter, retrn to 1 if at the end
    if($(obj).hasClass('last active')) {
      i = 1;
    } else {
      i++;
    }
    
  }, 5000);
}